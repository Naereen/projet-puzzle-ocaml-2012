(**	Projet Puzzle
	@author Lilian Besson <lilian.besson[AT]ens-cachan.fr> *)
	
open Resolution;;
open Interface;;	

print_string "Lancement de l'interface utilisateur en cours ...";;

Interface.interface_complete ();;
Graphics.close_graph ();;

(* Si on a pas lancé en mode toplevel, on quitte l'interpréteur dans le cas où il a été ouvert. (si on a lancé le programme par un 'monocaml -init interface.ml'). *)
if not(!(Sys.interactive)) then exit(0);;

print_string "Interface utilisateur du projet Puzzle : terminée.\n Lilian Besson (c) 2012";;

(** Aide de l'interface graphique : 

	Ce programme est écrit en Ocaml, et utilise le module graphique de la distribution standard Graphics pour l'affichage, et l'outil Zenity, disponible gratuitement sous Linux (''$ sudo apt-get install zenity'' pour le récupérer).

	Durant la boucle intéractive, les touches suivantes déclenchent les actions suivantes :
		h	:	affiche ce message d'aide,
		'enter' :	efface la fenêtre,
			'e'	fait la meme chose (le code de la touche 'enter' varie selon le mode d'exécution).
		u	:	change le nombre de couleurs utilisées,
		c	:	change l'échelle de couleur utilisée,
		m	:	change le nombre de colonnes des instances générées,
		n	:	change le nombre de lignes   des instances générées,
		'Escape':	quitte proprement et enregistre l'instance / le plateau courant dans un fichier de sauvegarde caché,
		i	:	sauvegarde l'image de la fenêtre dans une image JPEG externe, en demandant un nom de fichier (demande confirmation avant écrasement). Utilise l'outil 'import', disponible gratuitement sous Linux (''$ sudo apt-get install imagemagick'' pour le récupérer),
		s	:	sauvegarde l'instance / le plateau courant affiché à l'ecran dans un fichier texte externe, en demandant un nom de fichier (demande confirmation avant écrasement).
		b	:	ouvre une instance déjà sauvegardée, en utilisant le navigateur de document de Zenity.
		'space' :	génére une nouvelle instance aléatoire avec les paramètres courant, et l'affiche,
		w	:	génére une nouvelle instance aléatoire, ayant une solution, avec les paramètres courant, et l'affiche,
		y 	:	génére une solution aleatoire, avec les paramètres courant, et l'affiche,
		p	:	demande de choisir une pièce, puis une position pour cette pièce, et enfin confirmation pour la placer dans l'instance courante. Redessine celle ci ensuite. Un clic de la souris sur une position de pièce fait la même chose.
		v	:	pour vérifier la validité du plateau dessiné à l'écran, et afficher le résultat (valide, ou une preuve de non validité) dans une fenêtre informative.
		r	:	résoud l'instance courante affichée à l'écran,

	Appuyez sur Echap pour fermer cette fenêtre d'aide et retourner au programme. Evitez de quitter brutalement, la gestion souple d'une 'fatal error' n'est pas encore au point (ceci est valable pour tout le programme).
	*)